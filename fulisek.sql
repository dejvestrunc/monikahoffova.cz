-- Adminer 4.2.1 MySQL dump

SET NAMES utf8;
SET time_zone = '+00:00';
SET foreign_key_checks = 0;
SET sql_mode = 'NO_AUTO_VALUE_ON_ZERO';

DROP TABLE IF EXISTS `article`;
CREATE TABLE `article` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_czech_ci NOT NULL,
  `content` text COLLATE utf8_czech_ci NOT NULL,
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_czech_ci;

INSERT INTO `article` (`id`, `name`, `content`, `created`, `updated`) VALUES
(1,	'Goodbye #Brno!',	'<p>Po dlhom odhodlávaní som sa konečne rozhodla písať blog. Možno to bude tým, že sa v mojom živote konečne niečo deje a chcem to uchovať navždy aj v písanej podobe aby som si o 50 rokov mohla povedať, že to všetko naozaj stálo za to a môj pobyt na našej krásnej planéte mal zmysel.</p>\r\n\r\n<p>K tomuto kroku ma viedlo hlavne to, že o pár dní idem na Erasmus do vysnívaného Francúzska presnejšie Lyon. Sny si treba plniť a nie len sedieť na zadku! :)\r\nNo ešte predtým sa musím rozlúčiť na štyri mesiace so svojim milovaným Brníčkom a vydať sa na chvíľku domov preskúmať aké dobrodružstvá prinesie Ružomberok s mojou bitch Vaneskou, ktorú som dlho zanedbávala :( :D</p>\r\n\r\n<p>Ale tento prvý fucking amazing príspevok je hlavne o poďakovanie za posledné dva semestre strávené v Brne a poďakovanie ľuďom, s ktorými som si ho neskutočne užila!</p>\r\n\r\n<p>Stále neveríte na to, že všetko si do života priťahujete? Ja áno a preto som si pritiahla Scarlett. Ďakujem všetkým mojim strážnym anjelom, že jedno upršané popoludnie navrhla, že s nami bude bývať. Bolo to to najlepšie čo sa mohlo stať. Som neskutočne poctená, že som našla skoro takého retarda ako som ja. A totálne šťastná, že som stebou mohla zdielať Chill zónu, moje zmätené pocity, mimo stavy a ranné opice po prehýrenej noci :D Si kočka, si kočka si kočka a perfektne dokonalá! Uži si Belgicko a dones mi RUMové pralinky!</p>\r\n\r\n\r\n<p>Ďalšia čarovná osoba, ktorá mi ukázala svetlo v živote je moja Natálka. Jankina oslava nás spojila a odvtedy si navzájom spamujeme FB a zdielame naše bezvýchodiskové situácie. Bolo by veľmi vtipné opísať ich tu ale nechám to radšej tak lebo by si ma v noci podrezala a potom by si sa už nemala na koho sťažovať, že si mumle popod nos! :D Ty si moja spriaznená špekulantka. Nikdy nezabudem na horalku, kávenku, monte a nektarinku, ktorá je úplne božská hlavne preto lebo nemá chĺpky :D Sangriaaaa. Langoše. A nekončiace záchvaty smiechu. Ty ma nabíjaš energiou. Daj si pozor nech ťa v Grécku nepredajú aj s 23% daňou :D</p>\r\n\r\n<p>A baby ďakujem za Pohodu - osobitná kapitola, jediné šřastie, že my nie sme zasnúbené :D</p>\r\n\r\n<p>Malý človek, ktorý si rýchlo získa miesto vo vašom srdci to je Verunka - My wine queen. Teším sa na ďalší vínový piatok a nové drby :D A čakám ťa u nás - vlaky zadarmo musíš využiť! :D A držím palce ty moja Office manažérka!</p>\r\n\r\n<p>Ďakujem za najlepšie narodeniny, Čangovi a Dejvovi za mňamóznu tortu, KP za špekulantov a hudobné spestrenie života či už v jurte alebo na grilovačkách, Miškovi za morálnu podporu pri hľadaní životného partnera :D Gracias to my spanish boy et merci à bizzare Français. A všetkým za to, že ste. <3</p>\r\n\r\n\r\n<p>Je neskutočný počet ľudí, ktorý si tu zaslúžia byť tiež ale tieto tri považujem za najtop! Ostatní sa možno nájdete na fotke! :* XOXO</p>\r\n\r\n<p>SEE YOU IN JANUARY! :)</p>',	'2015-08-17 15:08:33',	'0000-00-00 00:00:00'),
(2,	'Test',	'fds',	'2015-08-17 09:24:50',	'0000-00-00 00:00:00');

DROP TABLE IF EXISTS `comment`;
CREATE TABLE `comment` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `aricle_id` int(11) NOT NULL,
  `comment_id` int(11) NOT NULL,
  `name` varchar(255) COLLATE utf8_czech_ci NOT NULL,
  `content` varchar(255) COLLATE utf8_czech_ci NOT NULL,
  `author` varchar(255) COLLATE utf8_czech_ci NOT NULL,
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`),
  KEY `aricle_id` (`aricle_id`),
  CONSTRAINT `comment_ibfk_1` FOREIGN KEY (`aricle_id`) REFERENCES `article` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_czech_ci;


DROP TABLE IF EXISTS `common_file`;
CREATE TABLE `common_file` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `link` varchar(255) COLLATE utf8_czech_ci NOT NULL,
  `name` varchar(255) COLLATE utf8_czech_ci NOT NULL,
  `description` varchar(255) COLLATE utf8_czech_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_czech_ci;


DROP TABLE IF EXISTS `file_connector`;
CREATE TABLE `file_connector` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `common_file_id` int(11) NOT NULL,
  `article_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `common_file_id` (`common_file_id`),
  KEY `article_id` (`article_id`),
  CONSTRAINT `file_connector_ibfk_1` FOREIGN KEY (`common_file_id`) REFERENCES `common_file` (`id`) ON DELETE CASCADE,
  CONSTRAINT `file_connector_ibfk_2` FOREIGN KEY (`article_id`) REFERENCES `article` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_czech_ci;


DROP TABLE IF EXISTS `hashtag`;
CREATE TABLE `hashtag` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `text` varchar(255) COLLATE utf8_czech_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_czech_ci;

INSERT INTO `hashtag` (`id`, `text`) VALUES
(1,	'Brno'),
(2,	'Holidays'),
(3,	'Friends');

DROP TABLE IF EXISTS `hashtag_connector`;
CREATE TABLE `hashtag_connector` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `article_id` int(11) NOT NULL,
  `hashtag_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `article_id` (`article_id`),
  KEY `hashtag_id` (`hashtag_id`),
  CONSTRAINT `hashtag_connector_ibfk_1` FOREIGN KEY (`article_id`) REFERENCES `article` (`id`) ON DELETE CASCADE,
  CONSTRAINT `hashtag_connector_ibfk_2` FOREIGN KEY (`hashtag_id`) REFERENCES `hashtag` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_czech_ci;

INSERT INTO `hashtag_connector` (`id`, `article_id`, `hashtag_id`) VALUES
(1,	1,	1),
(2,	1,	2),
(3,	1,	3);

DROP TABLE IF EXISTS `system_config`;
CREATE TABLE `system_config` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `key` varchar(255) COLLATE utf8_czech_ci NOT NULL,
  `description` varchar(255) COLLATE utf8_czech_ci NOT NULL,
  `value` varchar(255) COLLATE utf8_czech_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_czech_ci;

INSERT INTO `system_config` (`id`, `key`, `description`, `value`) VALUES
(1,	'PAGE_NAME',	'Jméno stránky',	'fulisek');

DROP TABLE IF EXISTS `users`;
CREATE TABLE `users` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `username` varchar(100) NOT NULL,
  `password` varchar(255) NOT NULL,
  `role` varchar(100) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

INSERT INTO `users` (`id`, `username`, `password`, `role`) VALUES
(6,	'admin',	'$2y$10$LhOZaZJr61SnQwp8E4B79OzoCysBnzmCMj0Rw2eWP8NrPqg0/uLM6',	'');

-- 2015-08-17 18:12:16
