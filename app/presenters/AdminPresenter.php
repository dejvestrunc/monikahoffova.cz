<?php

namespace App\Presenters;

use Nette;
use App\Model;
use Nette\Application\UI;
use Nette\Application\UI\Form;
use Nette\Application\Responses\JsonResponse;
use App\UserManager;
use Tracy\Debugger;
use Nette\Utils\Image;


class AdminPresenter extends BasePresenter
{
    /** @var \Service\RedactorJsUpload */
    private $redactorJsUpload;

    public $lang;
    public $article;
    public $about;
    public $project;
    public $file;

    public function __construct(Model\Article $article, Model\About $about, Model\Project $project, Model\File $file) {
        $this->article = $article;
        $this->about = $about;
        $this->project = $project;
        $this->file = $file;
    }

    public function beforeRender()
    {
        if (!$this->user->isLoggedIn()) {
            $this->redirect('Sign:in');
        }
        $this->template->lang = $this->lang;
    }

    public function renderDefault()
    {

    }

    public function renderPosts()
    {
        $projects = $this->project->getAllProjects()->fetchAll();
        $finalArrayOfProjects = [];

        foreach($projects as $key => $project){
            $finalArrayOfProjects[$key] = $project;
            $finalArrayOfProjects[$key]['images'] = $this->project->getProjectImagesByID($project['id'])->fetchAll();
        }

        //$projects = $this->project->getProjectImagesByID(1)->fetchAll();

        $this->template->projects = $finalArrayOfProjects;
    }

    public function renderArchivedPosts()
    {
        $inactiveProjects = $this->project->getAllInactiveProjects()->fetchAll();
        $finalArrayOfInactiveProjects = [];

        foreach($inactiveProjects as $key => $project){
            $finalArrayOfInactiveProjects[$key] = $project;
            $finalArrayOfInactiveProjects[$key]['images'] = $this->project->getProjectImagesByID($project['id'])->fetchAll();
        }

        //$projects = $this->project->getProjectImagesByID(1)->fetchAll();

        $this->template->inactiveProjects = $finalArrayOfInactiveProjects;
    }

    public function actionCreate()
    {

    }

    public function handleArchiveProject()
    {
        $this->project->updateProjectById($this->getRequest()->getPost('id'), array('active' => 0));
        $this->invalidateControl();
        $this->redrawControl();
        $this->redirect('Admin:posts');
    }

    public function handleUnarchiveProject()
    {
        $this->project->updateProjectById($this->getRequest()->getPost('id'), array('active' => 1));
        $this->invalidateControl();
        $this->redrawControl();
        $this->redirect('Admin:posts');
    }

    public function handleSwitchProjects(){
        $dragId = $this->getRequest()->getPost('dragId');
        $dropId = $this->getRequest()->getPost('dropId');

        $dragOrder = $this->getRequest()->getPost('dragOrder');
        $dropOrder = $this->getRequest()->getPost('dropOrder');

        $this->project->updateProjectById($dragId, array('order' => $dropOrder));
        $this->project->updateProjectById($dropId, array('order' => $dragOrder));
    }

    public function renderAbout()
    {
        $about = $this->about->getAbout();
        if ($about) {
            $this['aboutForm']->setDefaults(array(
                'text' => $about->text
            ));
        }
    }

    public function actionEdit($id)
    {
        $projects = $this->project->getProjectByID($id)->fetch();

        $this->template->images = $this->project->getProjectImagesByID($id)->fetchAll();
        $this->template->projectId = $id;

        $this['projectForm']->setDefaults($projects);
    }

    public function injectRedactorJsUpload(Model\RedactorJsUpload $upload)
    {
        $this->redactorJsUpload = $upload;
    }

    public function handleImageUpload()
    {
        $imageJson = $this->redactorJsUpload->imageUpload(
            $this->article->getUploadDir(),
            $this->getHttpRequest()->getFile('file')
        );
        $this->sendResponse(new JsonResponse($imageJson));
    }

    public function handleImagesJson()
    {
        $imagesJson = $this->redactorJsUpload->getImagesJson($this->article->getUploadDir());
        $this->sendResponse(new JsonResponse($imagesJson));
    }

    public function handleFileUpload()
    {
        $imageJson = $this->redactorJsUpload->fileUpload(
            $this->article->getUploadDir(),
            $this->getHttpRequest()->getFile('file')
        );
        $this->sendResponse(new JsonResponse($imageJson));
    }

    protected function createComponentProjectForm()
    {
        $form = new Form();
        $form->addText('name', 'Project name:')
            ->setAttribute('class', 'full-width')
            ->setRequired();
        $form->addSelect('category', 'Project category:')
            ->setItems(['school' => 'school', 'draw' => 'draw', 'photography' => 'photography'])
            ->setAttribute('class', 'full-width')
            ->setRequired();
        $form->addTextArea('description', 'Project description CZE')
            ->setAttribute('class', 'redactor full-width')
            ->setRequired();
        $form->addTextArea('description_en', 'Project description ENG')
            ->setAttribute('class', 'redactor full-width')
            ->setRequired();
        $form->addText('year', 'Year:')
            ->setAttribute('class', 'full-width')
            ->setRequired();
        $form->addSubmit('send', 'Public!');
        $form->onSuccess[] = array($this, 'projectFormSucceded');

        return $form;
    }

    public function projectFormSucceded($form, $values)
    {
        $projectId = $this->getParameter('id');

        if ($projectId) {
            $this->project->updateProjectById($projectId, (array) $values);
        } else {
            $this->project->createProject((array) $values);
        }
        $this->redirect('Admin:posts');
    }

    protected function createComponentAboutForm()
    {
        $form = new Form();
        $form->addTextArea('text', '')
            ->setAttribute('class', 'redactor full-width')
            ->setRequired();
        $form->addSubmit('send', 'Save!');
        $form->onSuccess[] = array($this, 'aboutFormSucceded');

        return $form;
    }

    public function aboutFormSucceded($form, $values)
    {
        $this->about->updateAbout($values);
        $this->redirect('Admin:about');
    }

    public function actionDelete($id = null)
    {
        $this->article->deleteArticleById($id);
        $this->redirect('Admin:posts');
    }

    public function handleUpload($projectId) {
        $file = $_FILES['file'];

        dump($file);
        dump($projectId);

        $imgPathTmp = $file['tmp_name'];
        $imgTmp = Image::fromFile($imgPathTmp);

        dump($imgTmp->getWidth());
        dump($imgTmp->getHeight());

        $imgId = $this->project->insertNewFile($projectId, $imgTmp->getWidth(), $imgTmp->getHeight());

        $imgPath = "images/projects/" . $imgId;

        $this->project->updateFileWithIdAndPath($imgId, $imgPath);

        $imgTmp->save("./" . $imgPath  . ".jpg", 100, Image::JPEG);
    }
}