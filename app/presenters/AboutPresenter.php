<?php

namespace App\Presenters;

use Nette;
use App\Model;


class AboutPresenter extends BasePresenter
{
	public $article;
    public $about;

	public function __construct(Model\Article $article, Model\About $about) {
		$this->article = $article;
        $this->about = $about;
	}

	public function renderDefault()
	{
		$about_array = [];
		$about = $this->about->getAbout($this->lang);

		foreach($about as $item){
			$about_array[$item['name']] = $item['text'];
		}

		$this->template->about_array = $about_array;
	}

}
