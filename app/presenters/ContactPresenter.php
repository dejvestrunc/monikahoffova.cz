<?php

namespace App\Presenters;

use Nette;
use App\Model;


class ContactPresenter extends BasePresenter
{
	public $article;
    public $about;

	public function __construct(Model\Article $article, Model\About $about) {
		$this->article = $article;
        $this->about = $about;
	}

	public function renderDefault()
	{

	}
}
