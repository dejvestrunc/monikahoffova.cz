<?php

namespace App\Presenters;

use Nette;
use App\Model;


/**
 * Base presenter for all application presenters.
 */
abstract class BasePresenter extends Nette\Application\UI\Presenter
{
    /** @persistent */
    public $lang;

    public function beforeRender(){
        if ($this->isAjax()) {
            $this->redrawControl('page_content');
        }
        $this->template->lang = $this->lang;
    }
}
