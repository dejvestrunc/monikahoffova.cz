<?php

namespace App\Presenters;

use Nette;
use App\Model;


class ProjectsPresenter extends BasePresenter
{
	public $article;
    public $about;
	public $project;
	public $file;

	public function __construct(Model\Article $article, Model\About $about, Model\Project $project, Model\File $file) {
		$this->article = $article;
        $this->about = $about;
		$this->project = $project;
		$this->file = $file;
	}

	public function renderDefault()
	{
		$projects = $this->project->getAllProjects()->fetchAll();
		$finalArrayOfProjects = [];

		foreach($projects as $key => $project){
			$finalArrayOfProjects[$key] = $project;
			$finalArrayOfProjects[$key]['images'] = $this->project->getProjectImagesByID($project['id'])->fetchAll();
		}

		//$projects = $this->project->getProjectImagesByID(1)->fetchAll();

		$this->template->projects = $finalArrayOfProjects;
	}

	public function renderProject($id = null)
	{
		if($id == null) {
			$this->template->project = array('name' => 'zirafy', 'img_url' => 'images/projects/srtsrtre.jpg');
		} else {
			$this->template->project = $this->project->getProjectByID($id)->fetch();
			$projects = $this->project->getAllProjectsForList()->fetchAll();

			foreach($projects as $arrayIndex => $project){
				if($project->id == $id){
					if($arrayIndex != 0) {
						$prev = $projects[$arrayIndex - 1];
					}else {
						$prev = $projects[sizeof($projects)-1];
					}
					if($arrayIndex == sizeof($projects)-1){
						$next = $projects[0];
					}else {
						$next = $projects[$arrayIndex + 1];
					}
				}
			}
			$this->template->nextProjectID = $next->id;
			$this->template->prevProjectID = $prev->id;
		}
	}
}
