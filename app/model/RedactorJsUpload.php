<?php

namespace App\Model;

use Nette\Utils\Image;

/**
 * RedactorJsUpload manage upload images and files for redactor.js
 *
 * @author Marek Lichtner
 */
class RedactorJsUpload {
	
	private $imagesDir = 'images';
	private $thumbSubDir = 'thumbs';
	private $filesDir = 'files';
	
	private $wwwDir, $basePath;
	
	function __construct($wwwDir, \Nette\Http\Request $request) {
		$this->wwwDir = $wwwDir;
		$this->basePath = substr($request->getUrl()->basePath, 0, -1);
	}
	
	function getImagesJson($uploadDir) {
		$imagesDir = $uploadDir . '/' . $this->imagesDir;
		$imagesJson = array();
		$path = "$this->wwwDir/$imagesDir/$this->thumbSubDir";
		foreach (glob("$path/*") as $file) {
			$filename = basename($file);
			$imagesJson[] = array(
				'thumb' => "$this->basePath/$imagesDir/$this->thumbSubDir/$filename",
				'image' => "$this->basePath/$imagesDir/$filename"
			);
		}
		return $imagesJson;
	}
	
	function imageUpload($uploadDir, \Nette\Http\FileUpload $file) {
		$imagesDir = $uploadDir . '/' . $this->imagesDir;
		$fullImageDir = "$this->wwwDir/$imagesDir";
		if (!file_exists($fullImageDir)) {
			mkdir("$fullImageDir/$this->thumbSubDir", 0777, true);
		}
		
		# move image
		$filename = time().'.jpg';
		$file->move("$fullImageDir/$filename");
		
		$image = Image::fromFile("$fullImageDir/$filename");
		# set max size
		$image->resize(1000, 700, $image::SHRINK_ONLY);
		$image->save("$fullImageDir/$filename");
		
		# create thumb 
		$image->resize(120, 120);
		$image->save("$fullImageDir/$this->thumbSubDir/$filename");
		
		return array('filelink' => "$this->basePath/$imagesDir/$filename");
	}
		
	
	function fileUpload($uploadDir, \Nette\Http\FileUpload $file) {
		$filesDir = $uploadDir . '/' . $this->filesDir;
		$fullFilesDir = "$this->wwwDir/$filesDir";
		if (!file_exists($fullFilesDir)) {
			mkdir("$fullFilesDir", 0777, true);
		}
		
		# move file
		$filename = strtolower($file->getSanitizedName());
		$file->move("$fullFilesDir/$filename");
		
		return array('filelink' => "$this->basePath/$filesDir/$filename");
	}
	
}
