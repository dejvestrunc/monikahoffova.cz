<?php

namespace App\Model;

use Nette;


/**
 * Users management.
 */
class SystemConfig extends BaseModel
{
    const
        TABLE_NAME = 'system_config',
        COLUMN_ID = 'id',
        COLUMN_KEY = 'key',
        COLUMN_DESCRIPTION = 'description',
        COLUMN_VALUE = 'value';


    public function getConfig($key = null) {
        return $this->database->table(self::TABLE_NAME)->where(self::COLUMN_KEY, $key)->fetch();
    }
}