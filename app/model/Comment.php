<?php

namespace App\Model;

use Nette;


/**
 * Users management.
 */
class Comment extends BaseModel
{
    const
        TABLE_NAME = 'comment',
        COLUMN_ID = 'id',
        COLUMN_NAME = 'name',
        COLUMN_CONTENT = 'content',
        COLUMN_AUTHOR = 'author',
        COLUMN_CREATED = 'created',
        COLUMN_UPDATED = 'updated';


}