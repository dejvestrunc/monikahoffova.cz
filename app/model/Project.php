<?php

namespace App\Model;

use Nette;


/**
 * Users management.
 */
class Project extends BaseModel
{
    const
        TABLE_NAME = 'project',
        COLUMN_ID = 'id',
        COLUMN_NAME = 'name',
        COLUMN_DESCRIPTION = 'description',
        COLUMN_LANG = 'lang',
        COLUMN_CATEGORY = 'category',
        COLUMN_LAYOUT = 'layout',
        COLUMN_HOVER = 'hover';

    public function getAllProjects() {
        $projects = $this->database->query('
          select * from `project`
          where project.active = 1
          order by project.order desc');
        return $projects;
    }

    public function getAllInactiveProjects() {
        $projects = $this->database->query('
          select * from `project`
          where project.active = 0
          order by project.order desc');
        return $projects;
    }

    public function getAllProjectsForList() {
        $projects = $this->database->query('
          select project.id, project.name, project.description, project.description_en, project.lang, project.category, project.layout, project.size, project.hover, project.year, project.order, file.link, file.width, file.height from `project`
          join project_has_file on project.id = project_has_file.project_id
          join file on file.id = project_has_file.file_id
          where project.category = "school"
          order by project.order desc');
        return $projects;
    }

    public function getProjectByID($id) {
        $project = $this->database->query('
          select project.id, project.name, project.description, project.description_en, project.lang, project.category, project.layout, project.size, project.hover, project.year, project.order, file.link, file.width, file.height from `project`
          join project_has_file on project.id = project_has_file.project_id
          join file on file.id = project_has_file.file_id
          where project.id = '.$id);
        return $project;
    }

    public function getProjectImagesByID($id){
        $projects = $this->database->query('
          select * from `file`
          join project_has_file on file.id = project_has_file.file_id
          where project_has_file.project_id = '.$id);
        return $projects;
    }

    public function updateProjectById($id, array $data) {
        $project = $this->database->table('project')->where('id', $id)->update($data);
        return $project;
    }

    public function createProject(array $data) {
        $project = $this->database->table('project')->insert($data);
        return $project;
    }

    public function insertNewFile($projectId, $width, $height) {
      $newId = $this->database->table('file')->insert(array(
        "width" => $width,
        "height" => $height
        ));

      $this->database->table('project_has_file')->insert(array(
        "project_id" => $projectId,
        "file_id" => $newId
        ));

      return $newId;
    }

    public function updateFileWithIdAndPath($fileId, $filePath) {
      $this->database->table('file')->where('id = ?', $fileId)->update(array(
        "link" => $filePath
        ));
    }
}