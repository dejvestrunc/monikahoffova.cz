<?php

namespace App\Model;

use Nette;


/**
 * Users management.
 */
class File extends BaseModel
{
    const
        TABLE_NAME = 'about',
        COLUMN_ID = 'id',
        COLUMN_NAME = 'text';

    public function getAbout() {
        return $this->database->table(self::TABLE_NAME)->where('lang', 'cz')->fetch();
    }

    public function updateAbout($values) {
        $text = $values['text'];
        $about = $this->getAbout();
        $about->update(array(
           'text' => $text
        ));
    }
}