<?php

namespace App\Model;

use Nette;


/**
 * Users management.
 */
class About extends BaseModel
{
    const
        TABLE_NAME = 'about',
        COLUMN_ID = 'id',
        COLUMN_NAME = 'name',
        COLUMN_LANG = 'lang',
        COLUMN_TEXT = 'text';

    public function getAbout($lang = null) {
        if($lang)
            return $this->database->table(self::TABLE_NAME)->where('lang', $lang)->fetchAll();
    }

    public function updateAbout($values) {
        $text = $values['text'];
        $about = $this->getAbout();
        $about->update(array(
           'text' => $text
        ));
    }
}