<?php

namespace App\Model;

use Nette;
use App\Model;


/**
 * Users management.
 */
class Article extends BaseModel
{
    const
        TABLE_NAME = 'article',
        COLUMN_ID = 'id',
        COLUMN_NAME = 'name',
        COLUMN_CONTENT = 'content',
        COLUMN_CREATED = 'created',
        COLUMN_UPDATED = 'updated';


    public function getArticles() {
        $articles = $this->database->table(self::TABLE_NAME)->order('created DESC')->fetchAll();
        return $articles;
    }

    public function getArticleById($id = null) {
        if($id != null) {
            return $this->database->table(self::TABLE_NAME)->get($id);
        }
    }

    public function getArticlesByHashtag($tag_id = null) {
        if($tag_id != null) {
            return $this->database->table(self::TABLE_NAME)->where(':hashtag_connector.hashtag_id', $tag_id)->fetchAll();
        }
    }

    public function getArticleHastags($article_id = null)
    {
        return $this->database->table('hashtag_connector')->where('article_id', $article_id);
    }

    public function createArticle($values)
    {
        $name = $values['name'];
        $content = $values['content'];
        $hashtags = $values['hashtags'];
        if($name != null && $content != null){
            $article = $this->database->table(self::TABLE_NAME)->insert(array(
                'name' => $name,
                'content' => $content
            ));

            $all_hashtags = self::map_hashtags($this->database->table('hashtag')->fetchAll());
            foreach ( explode(',',$hashtags) as $hashtag){
                $index = array_search(strtolower($hashtag), $all_hashtags);
                if($index){
                    $hashtag_connector = $this->database->table('hashtag_connector')->insert(array(
                        'hashtag_id' => $index,
                        'article_id' => $article->id
                    ));
                } else {
                    $hashtag = $this->database->table('hashtag')->insert(array(
                        'text' => strtolower($hashtag)
                    ));
                    $hashtag_connector = $this->database->table('hashtag_connector')->insert(array(
                        'hashtag_id' => $hashtag->id,
                        'article_id' => $article->id
                    ));
                }
            }
        }
    }

    public function updateArticle($articleId = null, $values)
    {
        $name = $values['name'];
        $content = $values['content'];
        $hashtags = $values['hashtags'];
        if($name != null && $content != null){
            $article = $this->getArticleById($articleId);
            $article->update(array(
                'name' => $name,
                'content' => $content
            ));

            $all_hashtags = self::map_hashtags($this->database->table('hashtag')->fetchAll());
            $this->database->table('hashtag_connector')->where('article_id', $articleId)->delete();
            foreach ( explode(',',$hashtags) as $hashtag){
                $index = array_search(strtolower($hashtag), $all_hashtags);
                if($index){
                    $hashtag_connector = $this->database->table('hashtag_connector')->insert(array(
                        'hashtag_id' => $index,
                        'article_id' => $article->id
                    ));
                } else {
                    //$hashtag = $this->database->table('hashtag')->where('text', $)
                    $hashtag = $this->database->table('hashtag')->insert(array(
                        'text' => strtolower($hashtag)
                    ));
                    $hashtag_connector = $this->database->table('hashtag_connector')->insert(array(
                        'hashtag_id' => $hashtag->id,
                        'article_id' => $article->id
                    ));
                }
            }
        }
    }

    public function deleteArticleById($id = null)
    {
        return $this->database->table(self::TABLE_NAME)->get($id)->delete();
    }

    function getUploadDir() {
        # e.g. something like this
        return "upload/articles/article";
    }


    //TOHLE BY MELO BYT V ARRAY HELPERU - DO BUDOUCNA DYNAMICKOU
    public function map_hashtags($array){
    $array_out = [];
    if($array != null){
        foreach($array as $object){
            $array_out[$object->id] = $object->text;
        }
    }
    return $array_out;
}
}