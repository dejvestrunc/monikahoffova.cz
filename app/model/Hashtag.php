<?php

namespace App\Model;

use Nette;


/**
 * Users management.
 */
class Hashtag extends BaseModel
{
    const
        TABLE_NAME = 'hashtag_connector',
        COLUMN_ID = 'id',
        COLUMN_NAME = 'text';

    public function getHashtags() {
        return $this->database->table(self::TABLE_NAME)->fetchAll();
    }

    public function getHashtagsByArticle($article_id = null) {
        return $this->database->table(self::TABLE_NAME)->where('article_id', $article_id)->fetchAll();
    }

    public function getHashtagsForUpdate($article_id = null){
        $hashtags = [];
        $pom = $this->database->table(self::TABLE_NAME)->where('article_id', $article_id)->fetchAll();
        foreach ($pom as $value){
            $hashtags[] = $value->hashtag->text;
        }
        return implode(',', $hashtags);
    }

    public function getHashtagByID($hashtag_id = null){
        return $this->database->table(self::TABLE_NAME)->get($hashtag_id);
    }
}