-- Adminer 4.2.1 MySQL dump

SET NAMES utf8;
SET time_zone = '+00:00';
SET foreign_key_checks = 0;
SET sql_mode = 'NO_AUTO_VALUE_ON_ZERO';

DROP TABLE IF EXISTS `about`;
CREATE TABLE `about` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `lang` varchar(255) NOT NULL,
  `text` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

INSERT INTO `about` (`id`, `name`, `lang`, `text`) VALUES
(1,	'bio',	'cs',	'<h2>bio</h2><hr><p>jméno: <b>Monika Hoffová</b></p><p>datum narození: <b>12. listopad 1993</b></p><p>bydliště: <b>Brno (CZ)</b></p><p>email: <b>monikahoffova@gmail.com</b></p>'),
(2,	'bio',	'en',	'<h2>bio</h2><hr><p>name: <b>Monika Hoffová</b></p><p>birthdate: <b>12 november 1993</b></p><p>place of residence: <b>Brno (CZ)</b></p><p>email: <b>monikahoffova@gmail.com</b></p>'),
(3,	'education',	'cs',	'<h2>vzdělání</h2>\r\n<hr>\r\n<p><b>2000 - 2009</b> Základní škola Mírové náměstí 19, Hodonín</p>\r\n<p><b>2009 - 2013</b> SŠPU A VOŠ  Hodonín, obor Pozemní stavitelství</p>\r\n<p><b>2013 - dosud</b> VUT Brno, Fakulta architektury</p>'),
(4,	'education',	'en',	'<h2>education</h2>\r\n<hr>\r\n<p><b>2000 - 2009</b> Elementary School Mírové náměstí 19, Hodonín</p>\r\n<p><b>2009 - 2013</b> SŠPU and VOŠ Hodonín, Faculty of Building construction</p>\r\n<p><b>2013 - dosud</b> VUT Brno, Faculty of Architecture</p>'),
(5,	'competition',	'cs',	'<h2>soutěže</h2>\r\n<hr>\r\n<p><b>2008</b> 2. místo ve fotografické soutěži Lidé</p>\r\n<p><b>2011</b> 1. místo v celostátní soutěži Malujeme barvami země</p>\r\n<p><b>2012</b> postup do celostátního kola soutěže Velux - Život pod střechou</p>\r\n<p><b>2012</b> 1. místo v soutěži o nejlepší školní projekt SŠPU Hodonín</p>'),
(6,	'competition',	'en',	'<h2>competition</h2><hr><p><b>2008</b> 2nd place in the photo competition – People</p><p><b>2011</b> 1st place in the national competition – Painting by natural colors</p><p><b>2012</b> national competition Velux - Life under the roof</p><p><b>2012</b> 1st place in the competition – best school project SŠPU Hodonín</p>'),
(7,	'design_skills',	'cs',	'<h2>dovednosti</h2>\r\n                    <hr>\r\n                    <table>\r\n                        <tr>\r\n                            <td>Adobe Photoshop</td>\r\n                            <td>\r\n                                <div class=\"dot\"></div>\r\n                                <div class=\"dot\"></div>\r\n                                <div class=\"dot\"></div>\r\n                                <div class=\"dot\"></div>\r\n                                <div class=\"dot\"></div>\r\n                                <div class=\"dot\"></div>\r\n                                <div class=\"dot\"></div>\r\n                                <div class=\"dot\"></div>\r\n                                <div class=\"dot-empty\"></div>\r\n                                <div class=\"dot-empty\"></div>\r\n                            </td>\r\n                        </tr>\r\n                        <tr>\r\n                            <td>Adobe Ilustrator</td>\r\n                            <td>\r\n                                <div class=\"dot\"></div>\r\n                                <div class=\"dot\"></div>\r\n                                <div class=\"dot\"></div>\r\n                                <div class=\"dot\"></div>\r\n                                <div class=\"dot\"></div>\r\n                                <div class=\"dot\"></div>\r\n                                <div class=\"dot\"></div>\r\n                                <div class=\"dot\"></div>\r\n                                <div class=\"dot-empty\"></div>\r\n                                <div class=\"dot-empty\"></div>\r\n                            </td>\r\n                        </tr>\r\n                        <tr>\r\n                            <td>Zoner photo studio</td>\r\n                            <td>\r\n                                <div class=\"dot\"></div>\r\n                                <div class=\"dot\"></div>\r\n                                <div class=\"dot\"></div>\r\n                                <div class=\"dot\"></div>\r\n                                <div class=\"dot\"></div>\r\n                                <div class=\"dot\"></div>\r\n                                <div class=\"dot\"></div>\r\n                                <div class=\"dot\"></div>\r\n                                <div class=\"dot\"></div>\r\n                                <div class=\"dot-empty\"></div>\r\n                            </td>\r\n                        </tr>\r\n                        <tr>\r\n                            <td>InDesign</td>\r\n                            <td>\r\n                                <div class=\"dot\"></div>\r\n                                <div class=\"dot\"></div>\r\n                                <div class=\"dot\"></div>\r\n                                <div class=\"dot\"></div>\r\n                                <div class=\"dot\"></div>\r\n                                <div class=\"dot\"></div>\r\n                                <div class=\"dot-empty\"></div>\r\n                                <div class=\"dot-empty\"></div>\r\n                                <div class=\"dot-empty\"></div>\r\n                                <div class=\"dot-empty\"></div>\r\n                            </td>\r\n                        </tr>\r\n                        <tr>\r\n                            <td>AutoCAD</td>\r\n                            <td>\r\n                                <div class=\"dot\"></div>\r\n                                <div class=\"dot\"></div>\r\n                                <div class=\"dot\"></div>\r\n                                <div class=\"dot\"></div>\r\n                                <div class=\"dot\"></div>\r\n                                <div class=\"dot\"></div>\r\n                                <div class=\"dot\"></div>\r\n                                <div class=\"dot\"></div>\r\n                                <div class=\"dot\"></div>\r\n                                <div class=\"dot-empty\"></div>\r\n                            </td>\r\n                        </tr>\r\n                        <tr>\r\n                            <td>AutoCAD Architecture</td>\r\n                            <td>\r\n                                <div class=\"dot\"></div>\r\n                                <div class=\"dot\"></div>\r\n                                <div class=\"dot\"></div>\r\n                                <div class=\"dot\"></div>\r\n                                <div class=\"dot\"></div>\r\n                                <div class=\"dot\"></div>\r\n                                <div class=\"dot\"></div>\r\n                                <div class=\"dot\"></div>\r\n                                <div class=\"dot\"></div>\r\n                                <div class=\"dot-empty\"></div>\r\n                            </td>\r\n                        </tr>\r\n                        <tr>\r\n                            <td>Archicad</td>\r\n                            <td>\r\n                                <div class=\"dot\"></div>\r\n                                <div class=\"dot\"></div>\r\n                                <div class=\"dot\"></div>\r\n                                <div class=\"dot\"></div>\r\n                                <div class=\"dot\"></div>\r\n                                <div class=\"dot\"></div>\r\n                                <div class=\"dot\"></div>\r\n                                <div class=\"dot\"></div>\r\n                                <div class=\"dot\"></div>\r\n                                <div class=\"dot-empty\"></div>\r\n                            </td>\r\n                        </tr>\r\n                        <tr>\r\n                            <td>SketchUp</td>\r\n                            <td>\r\n                                <div class=\"dot\"></div>\r\n                                <div class=\"dot\"></div>\r\n                                <div class=\"dot\"></div>\r\n                                <div class=\"dot\"></div>\r\n                                <div class=\"dot\"></div>\r\n                                <div class=\"dot\"></div>\r\n                                <div class=\"dot\"></div>\r\n                                <div class=\"dot\"></div>\r\n                                <div class=\"dot\"></div>\r\n                                <div class=\"dot\"></div>\r\n                            </td>\r\n                        </tr>\r\n                        <tr>\r\n                            <td>Microsoft Office</td>\r\n                            <td>\r\n                                <div class=\"dot\"></div>\r\n                                <div class=\"dot\"></div>\r\n                                <div class=\"dot\"></div>\r\n                                <div class=\"dot\"></div>\r\n                                <div class=\"dot\"></div>\r\n                                <div class=\"dot\"></div>\r\n                                <div class=\"dot\"></div>\r\n                                <div class=\"dot\"></div>\r\n                                <div class=\"dot\"></div>\r\n                                <div class=\"dot\"></div>\r\n                            </td>\r\n                        </tr>\r\n                    </table>'),
(8,	'design_skills',	'en',	'<h2>design skills</h2>\r\n                    <hr>\r\n                    <table>\r\n                        <tr>\r\n                            <td>Adobe Photoshop</td>\r\n                            <td>\r\n                                <div class=\"dot\"></div>\r\n                                <div class=\"dot\"></div>\r\n                                <div class=\"dot\"></div>\r\n                                <div class=\"dot\"></div>\r\n                                <div class=\"dot\"></div>\r\n                                <div class=\"dot\"></div>\r\n                                <div class=\"dot\"></div>\r\n                                <div class=\"dot\"></div>\r\n                                <div class=\"dot-empty\"></div>\r\n                                <div class=\"dot-empty\"></div>\r\n                            </td>\r\n                        </tr>\r\n                        <tr>\r\n                            <td>Adobe Ilustrator</td>\r\n                            <td>\r\n                                <div class=\"dot\"></div>\r\n                                <div class=\"dot\"></div>\r\n                                <div class=\"dot\"></div>\r\n                                <div class=\"dot\"></div>\r\n                                <div class=\"dot\"></div>\r\n                                <div class=\"dot\"></div>\r\n                                <div class=\"dot\"></div>\r\n                                <div class=\"dot\"></div>\r\n                                <div class=\"dot-empty\"></div>\r\n                                <div class=\"dot-empty\"></div>\r\n                            </td>\r\n                        </tr>\r\n                        <tr>\r\n                            <td>Zoner photo studio</td>\r\n                            <td>\r\n                                <div class=\"dot\"></div>\r\n                                <div class=\"dot\"></div>\r\n                                <div class=\"dot\"></div>\r\n                                <div class=\"dot\"></div>\r\n                                <div class=\"dot\"></div>\r\n                                <div class=\"dot\"></div>\r\n                                <div class=\"dot\"></div>\r\n                                <div class=\"dot\"></div>\r\n                                <div class=\"dot\"></div>\r\n                                <div class=\"dot-empty\"></div>\r\n                            </td>\r\n                        </tr>\r\n                        <tr>\r\n                            <td>InDesign</td>\r\n                            <td>\r\n                                <div class=\"dot\"></div>\r\n                                <div class=\"dot\"></div>\r\n                                <div class=\"dot\"></div>\r\n                                <div class=\"dot\"></div>\r\n                                <div class=\"dot\"></div>\r\n                                <div class=\"dot\"></div>\r\n                                <div class=\"dot-empty\"></div>\r\n                                <div class=\"dot-empty\"></div>\r\n                                <div class=\"dot-empty\"></div>\r\n                                <div class=\"dot-empty\"></div>\r\n                            </td>\r\n                        </tr>\r\n                        <tr>\r\n                            <td>AutoCAD</td>\r\n                            <td>\r\n                                <div class=\"dot\"></div>\r\n                                <div class=\"dot\"></div>\r\n                                <div class=\"dot\"></div>\r\n                                <div class=\"dot\"></div>\r\n                                <div class=\"dot\"></div>\r\n                                <div class=\"dot\"></div>\r\n                                <div class=\"dot\"></div>\r\n                                <div class=\"dot\"></div>\r\n                                <div class=\"dot\"></div>\r\n                                <div class=\"dot-empty\"></div>\r\n                            </td>\r\n                        </tr>\r\n                        <tr>\r\n                            <td>AutoCAD Architecture</td>\r\n                            <td>\r\n                                <div class=\"dot\"></div>\r\n                                <div class=\"dot\"></div>\r\n                                <div class=\"dot\"></div>\r\n                                <div class=\"dot\"></div>\r\n                                <div class=\"dot\"></div>\r\n                                <div class=\"dot\"></div>\r\n                                <div class=\"dot\"></div>\r\n                                <div class=\"dot\"></div>\r\n                                <div class=\"dot\"></div>\r\n                                <div class=\"dot-empty\"></div>\r\n                            </td>\r\n                        </tr>\r\n                        <tr>\r\n                            <td>Archicad</td>\r\n                            <td>\r\n                                <div class=\"dot\"></div>\r\n                                <div class=\"dot\"></div>\r\n                                <div class=\"dot\"></div>\r\n                                <div class=\"dot\"></div>\r\n                                <div class=\"dot\"></div>\r\n                                <div class=\"dot\"></div>\r\n                                <div class=\"dot\"></div>\r\n                                <div class=\"dot\"></div>\r\n                                <div class=\"dot\"></div>\r\n                                <div class=\"dot-empty\"></div>\r\n                            </td>\r\n                        </tr>\r\n                        <tr>\r\n                            <td>SketchUp</td>\r\n                            <td>\r\n                                <div class=\"dot\"></div>\r\n                                <div class=\"dot\"></div>\r\n                                <div class=\"dot\"></div>\r\n                                <div class=\"dot\"></div>\r\n                                <div class=\"dot\"></div>\r\n                                <div class=\"dot\"></div>\r\n                                <div class=\"dot\"></div>\r\n                                <div class=\"dot\"></div>\r\n                                <div class=\"dot\"></div>\r\n                                <div class=\"dot\"></div>\r\n                            </td>\r\n                        </tr>\r\n                        <tr>\r\n                            <td>Microsoft Office</td>\r\n                            <td>\r\n                                <div class=\"dot\"></div>\r\n                                <div class=\"dot\"></div>\r\n                                <div class=\"dot\"></div>\r\n                                <div class=\"dot\"></div>\r\n                                <div class=\"dot\"></div>\r\n                                <div class=\"dot\"></div>\r\n                                <div class=\"dot\"></div>\r\n                                <div class=\"dot\"></div>\r\n                                <div class=\"dot\"></div>\r\n                                <div class=\"dot\"></div>\r\n                            </td>\r\n                        </tr>\r\n                    </table>'),
(9,	'hobby',	'cs',	'<h2>zájmy</h2>\r\n                    <hr>\r\n                    <div class=\"hobby-wrapper\">\r\n                        <div class=\"hobby-item\"><img src=\"/images/pictograms/design.png\" width=\"80\"><p>intérierový design</p></div>\r\n                        <div class=\"hobby-item\"><img src=\"/images/pictograms/exhibitions.png\" width=\"80\"><p>výstavy</p></div>\r\n                        <div class=\"hobby-item\"><img src=\"/images/pictograms/fashion.png\" width=\"80\"><p>móda</p></div>\r\n                        <div class=\"hobby-item\"><img src=\"/images/pictograms/graphic.png\" width=\"80\"><p>grafický design</p></div>\r\n                        <div class=\"hobby-item\"><img src=\"/images/pictograms/hockey.png\" width=\"80\"><p>sport pasivně</p></div>\r\n                        <div class=\"hobby-item\"><img src=\"/images/pictograms/pet.png\" width=\"80\"><p>zvířata</p></div>\r\n                        <div class=\"hobby-item\"><img src=\"/images/pictograms/photo.png\" width=\"80\"><p>fotografování</p></div>\r\n                        <div class=\"hobby-item\"><img src=\"/images/pictograms/tenis.png\" width=\"80\"><p>sport aktivně</p></div>\r\n                        <div class=\"hobby-item\"><img src=\"/images/pictograms/video.png\" width=\"80\"><p>filmy</p></div>\r\n                    </div>'),
(10,	'hobby',	'en',	'<h2>hobbies</h2>\r\n                    <hr>\r\n                    <div class=\"hobby-wrapper\">\r\n                        <div class=\"hobby-item\"><img src=\"/images/pictograms/design.png\" width=\"80\"><p>interior design</p></div>\r\n                        <div class=\"hobby-item\"><img src=\"/images/pictograms/exhibitions.png\" width=\"80\"><p>exhibitions</p></div>\r\n                        <div class=\"hobby-item\"><img src=\"/images/pictograms/fashion.png\" width=\"80\"><p>fashion</p></div>\r\n                        <div class=\"hobby-item\"><img src=\"/images/pictograms/graphic.png\" width=\"80\"><p>graphic design</p></div>\r\n                        <div class=\"hobby-item\"><img src=\"/images/pictograms/hockey.png\" width=\"80\"><p>passive sport</p></div>\r\n                        <div class=\"hobby-item\"><img src=\"/images/pictograms/pet.png\" width=\"80\"><p>animals</p></div>\r\n                        <div class=\"hobby-item\"><img src=\"/images/pictograms/photo.png\" width=\"80\"><p>photography</p></div>\r\n                        <div class=\"hobby-item\"><img src=\"/images/pictograms/tenis.png\" width=\"80\"><p>active sport</p></div>\r\n                        <div class=\"hobby-item\"><img src=\"/images/pictograms/video.png\" width=\"80\"><p>movies</p></div>\r\n                    </div>'),
(11,	'other_skills',	'cs',	'<h2>další dovednosti</h2>\r\n<hr>\r\n<p><b>Řidičský průkaz</b> skupina B (2012), aktivní řidič</p>'),
(12,	'other_skills',	'en',	'<h2>other skills</h2>\r\n<hr>\r\n<p><b>Driving license</b> - type B (2012), active driver</p>');

DROP TABLE IF EXISTS `file`;
CREATE TABLE `file` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(45) NOT NULL,
  `link` varchar(45) NOT NULL,
  `type` varchar(45) DEFAULT NULL,
  `width` int(11) DEFAULT NULL,
  `height` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

INSERT INTO `file` (`id`, `name`, `link`, `type`, `width`, `height`) VALUES
(1,	'atelier',	'images/projects/project1/title',	NULL,	1500,	1500),
(2,	'spolecny atelier',	'images/projects/project2/title',	NULL,	1500,	1500),
(3,	'tapeta navrh',	'images/projects/project3/title',	NULL,	1500,	1500),
(5,	'img2',	'images/projects/photo/img2',	NULL,	513,	513),
(8,	'img5',	'images/projects/photo/img5',	NULL,	892,	892),
(9,	'img6',	'images/projects/photo/img6',	NULL,	894,	894),
(10,	'img7',	'images/projects/photo/img7',	NULL,	896,	896),
(12,	'img9',	'images/projects/photo/img9',	NULL,	892,	892),
(13,	'img10',	'images/projects/photo/img10',	NULL,	932,	932),
(15,	'img12',	'images/projects/photo/img12',	NULL,	890,	890),
(17,	'atelier',	'images/projects/project1/1',	NULL,	1500,	1500),
(18,	'atelier',	'images/projects/project1/2',	NULL,	1500,	1500),
(19,	'atelier',	'images/projects/project1/3',	NULL,	1500,	1500),
(20,	'atelier',	'images/projects/project1/4',	NULL,	1500,	1500),
(21,	'atelier',	'images/projects/project1/5',	NULL,	1500,	1500),
(22,	'atelier',	'images/projects/project1/6',	NULL,	1500,	1500),
(23,	'atelier',	'images/projects/project1/7',	NULL,	1500,	1500),
(24,	'atelier',	'images/projects/project1/8',	NULL,	1500,	1500),
(25,	'atelier',	'images/projects/project1/9',	NULL,	1500,	1500),
(26,	'atelier',	'images/projects/project1/10',	NULL,	1500,	1500),
(27,	'atelier',	'images/projects/project1/11',	NULL,	1500,	1500),
(28,	'galaxie',	'images/projects/galaxie/title',	NULL,	1500,	1500),
(29,	'lebka',	'images/projects/lebka/title',	NULL,	1500,	1500),
(30,	'plakát',	'images/projects/plakat/title',	NULL,	1075,	1500),
(31,	'žárlivost',	'images/projects/zarlivost/title',	NULL,	2081,	1500),
(32,	'žena',	'images/projects/zena/title',	NULL,	1500,	1500),
(33,	'galaxie',	'images/projects/galaxie/1',	NULL,	1500,	1500),
(34,	'galaxie',	'images/projects/galaxie/2',	NULL,	1500,	1500),
(35,	'zimoviste',	'images/projects/zimoviste/title',	NULL,	1050,	1500),
(36,	'zimoviste',	'images/projects/zimoviste/1',	NULL,	1050,	1500),
(37,	'zimoviste',	'images/projects/zimoviste/2',	NULL,	1500,	1086),
(38,	'zimoviste',	'images/projects/zimoviste/3',	NULL,	1500,	1086),
(39,	'zimoviste',	'images/projects/zimoviste/4',	NULL,	1500,	1086),
(40,	'zimoviste',	'images/projects/zimoviste/5',	NULL,	1500,	1086),
(41,	'zimoviste',	'images/projects/zimoviste/6',	NULL,	1500,	1086),
(42,	'zimoviste',	'images/projects/zimoviste/7',	NULL,	1500,	1086),
(43,	'zimoviste',	'images/projects/zimoviste/8',	NULL,	1500,	1086),
(44,	'zidle',	'images/projects/zidle/title',	NULL,	1060,	1590),
(45,	'zidle',	'images/projects/zidle/1',	NULL,	1060,	1500),
(46,	'zidle',	'images/projects/zidle/2',	NULL,	1500,	1061),
(47,	'zidle',	'images/projects/zidle/3',	NULL,	1500,	1061),
(48,	'zidle',	'images/projects/zidle/4',	NULL,	1500,	1061),
(49,	'tapeta',	'images/projects/project3/1',	NULL,	1500,	1500),
(50,	'tapeta',	'images/projects/project3/2',	NULL,	1500,	1500),
(51,	'tapeta',	'images/projects/project3/3',	NULL,	1500,	1500),
(52,	'atelier_spolecny',	'images/projects/project2/1',	NULL,	1500,	1500),
(53,	'atelier_spolecny',	'images/projects/project2/2',	NULL,	1500,	1500),
(54,	'atelier_spolecny',	'images/projects/project2/3',	NULL,	1500,	1500),
(55,	'atelier_spolecny',	'images/projects/project2/4',	NULL,	1500,	1500),
(56,	'atelier_spolecny',	'images/projects/project2/5',	NULL,	1500,	1500),
(57,	'hodonin nabrezi',	'images/projects/hodonin_nabrezi/title',	NULL,	1500,	1500),
(58,	'hodonin nabrezi',	'images/projects/hodonin_nabrezi/1',	NULL,	1050,	1500),
(59,	'hodonin nabrezi',	'images/projects/hodonin_nabrezi/2',	NULL,	1500,	1500),
(60,	'hodonin nabrezi',	'images/projects/hodonin_nabrezi/3',	NULL,	1500,	1500),
(61,	'hodonin nabrezi',	'images/projects/hodonin_nabrezi/4',	NULL,	1500,	1500),
(62,	'hodonin nabrezi',	'images/projects/hodonin_nabrezi/5',	NULL,	1500,	1500),
(63,	'hodonin nabrezi',	'images/projects/hodonin_nabrezi/6',	NULL,	1500,	1500),
(64,	'hodonin nabrezi',	'images/projects/hodonin_nabrezi/7',	NULL,	1500,	1500),
(65,	'hodonin nabrezi',	'images/projects/hodonin_nabrezi/8',	NULL,	1500,	1500),
(66,	'hodonin nabrezi',	'images/projects/hodonin_nabrezi/9',	NULL,	1500,	1500),
(67,	'hodonin nabrezi',	'images/projects/hodonin_nabrezi/10',	NULL,	1500,	1500),
(68,	'moris',	'images/projects/photo/img14',	NULL,	600,	600),
(69,	'model',	'images/projects/model/title',	NULL,	720,	720);

DROP TABLE IF EXISTS `project`;
CREATE TABLE `project` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `description` text NOT NULL,
  `description_en` text NOT NULL,
  `lang` varchar(45) NOT NULL DEFAULT 'cz',
  `category` varchar(45) NOT NULL,
  `layout` int(11) NOT NULL DEFAULT '1',
  `hover` varchar(45) DEFAULT 'roxy',
  `year` varchar(45) NOT NULL,
  `size` int(11) NOT NULL DEFAULT '1',
  `order` int(11) NOT NULL,
  `active` tinyint(4) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

INSERT INTO `project` (`id`, `name`, `description`, `description_en`, `lang`, `category`, `layout`, `hover`, `year`, `size`, `order`, `active`) VALUES
(1,	'ateliér',	'Urbanisticky řešená část BVV. Návrh komplexu 8 bytových domů.',	'Urban solution of the BVV. Draft complex of eight residential buildings.',	'cz',	'school',	1,	'roxy',	'2015',	1,	6,	1),
(2,	'společný ateliér',	'Urbanisticky řešená část BVV. Společný ateliér.',	'Urban solution of the BVV. Common.',	'cz',	'school',	1,	'roxy',	'2015',	1,	9,	1),
(3,	'tapeta návrh',	'<p>Návrh tapety</p>',	'<p>Draft wallpaper</p>',	'cz',	'school',	1,	'roxy',	'2015',	1,	19,	1),
(5,	'galaxie',	'Galaxie - detail, olejomalba',	'Galaxy - detail, oil',	'cz',	'draw',	1,	'roxy',	'2015',	1,	5,	1),
(6,	'lebka',	'Lebka, hlína - hodinovka',	'Skull, clay - hour',	'cz',	'draw',	1,	'roxy',	'2015',	1,	16,	1),
(7,	'plakat',	'Plakát, malba + pc úpravy',	'Poster, painting + pc adjustments',	'cz',	'draw',	1,	'roxy',	'2015',	1,	3,	1),
(8,	'žárlivost',	'<p>7. smrtelných hříchů - žárlivost, malba</p>',	'<p>7th Deadly Sins - jealousy, painting</p>',	'cz',	'draw',	1,	'roxy',	'2015',	1,	13,	1),
(9,	'žena',	'Žena, rychlokresba',	'Woman, speedpaint',	'cz',	'draw',	1,	'roxy',	'2015',	1,	17,	1),
(15,	'img6',	'',	'',	'cz',	'photography',	1,	'roxy',	'2015',	1,	12,	1),
(16,	'img7',	'',	'',	'cz',	'photography',	1,	'roxy',	'2015',	1,	8,	1),
(19,	'img10',	'',	'',	'cz',	'photography',	1,	'roxy',	'2015',	1,	14,	1),
(23,	'zimoviště',	'Přestavba propanbutanové stanice na zimoviště žiraf',	'Conversion of propane butane station for wintering giraffes',	'cz',	'school',	1,	'roxy',	'2015',	2,	11,	1),
(24,	'židle',	'Židle realizace',	'Chair implementation',	'cz',	'school',	1,	'roxy',	'2015',	3,	18,	1),
(25,	'hodonín nábřeží',	'Návrh nového nábřeží v Hodoníně',	'Proposal for a new waterfront in Hodonin',	'cz',	'school',	1,	'roxy',	'2015',	1,	15,	1),
(26,	'img14',	'',	'',	'cz',	'photography',	1,	'roxy',	'2015',	1,	1,	1),
(27,	'model',	'Papírový struktivní model – The Cathedral of Christ the light, Oakland',	'Paper structive model - The Cathedral of Christ the Light, Oakland',	'cz',	'school',	1,	'roxy',	'2015',	1,	2,	1);

DROP TABLE IF EXISTS `project_has_file`;
CREATE TABLE `project_has_file` (
  `project_id` int(10) unsigned NOT NULL,
  `file_id` int(10) unsigned NOT NULL,
  PRIMARY KEY (`project_id`,`file_id`),
  KEY `fk_project_has_file_file1_idx` (`file_id`),
  KEY `fk_project_has_file_project_idx` (`project_id`),
  CONSTRAINT `fk_project_has_file_file1` FOREIGN KEY (`file_id`) REFERENCES `file` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_project_has_file_project` FOREIGN KEY (`project_id`) REFERENCES `project` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

INSERT INTO `project_has_file` (`project_id`, `file_id`) VALUES
(1,	1),
(2,	2),
(3,	3),
(15,	9),
(16,	10),
(19,	13),
(1,	17),
(1,	18),
(1,	19),
(1,	20),
(1,	21),
(1,	22),
(1,	23),
(1,	24),
(1,	25),
(1,	26),
(1,	27),
(5,	28),
(6,	29),
(7,	30),
(8,	31),
(9,	32),
(5,	33),
(5,	34),
(23,	35),
(23,	37),
(23,	38),
(23,	39),
(23,	40),
(23,	41),
(23,	42),
(23,	43),
(24,	44),
(24,	46),
(24,	47),
(24,	48),
(3,	49),
(3,	50),
(3,	51),
(2,	52),
(2,	53),
(2,	54),
(2,	55),
(2,	56),
(25,	57),
(25,	58),
(25,	59),
(25,	60),
(25,	61),
(25,	62),
(25,	63),
(25,	64),
(25,	65),
(25,	66),
(25,	67),
(26,	68),
(27,	69);

DROP TABLE IF EXISTS `users`;
CREATE TABLE `users` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `username` varchar(100) NOT NULL,
  `password` varchar(255) NOT NULL,
  `role` varchar(100) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

INSERT INTO `users` (`id`, `username`, `password`, `role`) VALUES
(6,	'admin',	'$2y$10$LhOZaZJr61SnQwp8E4B79OzoCysBnzmCMj0Rw2eWP8NrPqg0/uLM6',	'');

-- 2016-05-01 09:56:20
