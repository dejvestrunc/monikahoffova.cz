var windowWidth;
var columnWidth;
var imageMap;
var numOfColumns;
var numOfRows;
var animSpeed = 300;

$(document).ready(function() {
	//console.log('ready');
	var a;

    $(window).resize(function() {
        clearTimeout(a);

        a = setTimeout(function() {
            calculatePositions();
        }, 200);
    });
});

function calculatePositions() {
	windowWidth = $('#gallery').innerWidth();

	//console.log("Width: " + windowWidth);

	numOfColumns = Math.ceil(windowWidth / 300);

	numOfColumns = numOfColumns < 2 ? 2 : numOfColumns;

	columnWidth = windowWidth / numOfColumns;

	var images = $('.gallery-img').not('.display-none');
	images.sort(sortByOrder);

	//console.log(images);

	var count = images.length;

	//console.log("Number of images: " + count);

	numOfRows = Math.ceil(count / numOfColumns);
	numOfRows += 2;

	//console.log("Number of rows: " + numOfRows);

	imageMap = [];

	for (var i = 0; i < numOfRows; i++) {
		var imageMapRow = [];

		for (var j = 0; j < numOfColumns; j++) {
			imageMapRow.push(0);
		}

		imageMap.push(imageMapRow);
	}

	//console.log(imageMap);

	images.each(function() {
		var size = getSize($(this));

		var position = findFreeSpot(size);

		if (position) {
			//console.log(position);
			if(size != 3){
				$(this).width(columnWidth * size);
				$(this).height(columnWidth * size);
			} else {
				$(this).width((columnWidth * size * 2)/3);
				$(this).height(columnWidth * size);
			}
			$(this).animate({"left": position[0] * columnWidth, "top": position[1] * columnWidth}, animSpeed);
		} else {
			console.log("ERROR!!!!");
		}
	});
}

function findFreeSpot(size) {
	switch(size) {
		case 1: {
			for (var i = 0; i < numOfRows; i++) {
				for (var j = 0; j < numOfColumns; j++) {
					var state = imageMap[i][j];

					if (state == 0) {
						imageMap[i][j] = 1;

						return [j, i];
					}
				}
			}

			break;
		}
		case 2: {
			for (var i = 0; i < numOfRows - (size - 1); i++) {
				for (var j = 0; j < numOfColumns - (size - 1); j++) {
					var state1 = imageMap[i][j];
					var state2 = imageMap[i + 1][j];
					var state3 = imageMap[i][j + 1];
					var state4 = imageMap[i + 1][j + 1];

					if ((state1 == 0) && (state2 == 0) && (state3 == 0) && (state4 == 0)) {
						imageMap[i][j] = 1;
						imageMap[i + 1][j] = 1;
						imageMap[i][j + 1] = 1;
						imageMap[i + 1][j + 1] = 1;

						return [j, i];
					}
				}
			}

			break;
		}
		case 3: {
			for (var i = 0; i < numOfRows - (size - 1); i++) {
				for (var j = 0; j < numOfColumns - (size - 2); j++) {
					var state1 = imageMap[i][j];
					var state2 = imageMap[i + 1][j];
					var state3 = imageMap[i][j + 1];
					var state4 = imageMap[i + 1][j + 1];
					var state5 = imageMap[i + 2][j];
					var state6 = imageMap[i + 2][j + 1];

					if ((state1 == 0) && (state2 == 0) && (state3 == 0) && (state4 == 0) && (state5 == 0) && (state6 == 0)) {
						imageMap[i][j] = 1;
						imageMap[i + 1][j] = 1;
						imageMap[i][j + 1] = 1;
						imageMap[i + 1][j + 1] = 1;
						imageMap[i + 2][j] = 1;
						imageMap[i + 2][j + 1] = 1;

						return [j, i];
					}
				}
			}

			break;
		}
	}
	
	console.log(imageMap);

	numOfRows += 2;

	for (var k = 0; k < 2; k++) {
		var imageMapRow = [];

		for (var l = 0; l < numOfColumns; l++) {
			imageMapRow.push(0);
		}

		imageMap.push(imageMapRow);
	}

	return findFreeSpot(size);
}

function getSize(obj) {
	if (obj.attr("data-size") == "1") {
		return 1;
	} else if(obj.attr("data-size") == "2") {
		return 2;
	} else if(obj.attr("data-size") == "3"){
		return 3;
	}
}

function sortByOrder(a, b) {
	var aOrder = parseInt($(a).attr("order"));
	var bOrder = parseInt($(b).attr("order"));

	return ((aOrder < bOrder) ? -1 : ((aOrder > bOrder) ? 1 : 0));
}