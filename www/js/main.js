var hidden = true;
var scrolling = false;
var fade_duration = 1000;
var enable_to_open = true;
var menu_init = false;
var categories = {school:false, draw:false, photography:false};
var years = {y2012:false, y2013:false, y2014:false, y2015:false};
var selected_category;
var timeout;

$(function(){
    $.nette.ext('history').cache = false;
    $.nette.ext({
        init: function(){
            menu_init = true;
        },
        start: function(){
            enable_to_open = false;
            if(!hidden){
            }
        },
        complete: function () {
            enable_to_open = true;
            if (menu_init == false){
                setTimeout(function (){
                    if(!hidden)
                        menuAnimation();
                }, 200);
            }
        }
    });
    $.nette.init();

    $('.project-item').on('click', function(){
        console.log('projects clicked');
    });

    $('#nav-icon4, .clone').on('click mouseenter', function(){
        menuAnimation();
    });
});


function menuAnimation(){
    if(enable_to_open){
        $('#nav-icon4').toggleClass('open');
        $('#page').toggleClass('open-menu');
        if(hidden){
            $('body').css('overflow', 'hidden');
            $('.clone').fadeIn();
            hidden = false;
        } else {
            setTimeout(function(){
                $('body').css('overflow', 'auto');
            }, 350);
            $('.clone').fadeOut();
            hidden = true;
        }
        enable_to_open = false;
        setTimeout(function(){
            enable_to_open = true;
            menu_init = false;
        }, 350);
    }
    return 1;
}

