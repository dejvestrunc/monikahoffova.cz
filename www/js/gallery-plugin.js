function galleryInit(){
    $('.gallery-img[project-category*=photography]').on('click', function(){
        console.log('clicked on photography');
        console.log($(this).context.src);
        $('#image-preview').attr('src', $(this).context.src);
        updateImage();
        $('.gallery-clone').fadeIn();
    });

    $('.gallery-clone').on('click', function(){
        console.log('clicked on clone');
        $(this).fadeOut();
    });

    $('#image-preview').on('swipe', function(){
        console.log('swipe');
    });
}


function updateImage(){
    var window_height = $(window).height();
    var window_width = $(window).width();

    setTimeout(function(){
        var image_height = $('#image-preview').height();
        var image_width = $('#image-preview').width();

        $('#image-preview').css('left', window_width/2 - image_width/2);
        $('#image-preview').css('top', window_height/2 - image_height/2);
    }, 200);
}